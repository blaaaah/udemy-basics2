import React from 'react';

const Validation = (props) => {

    let message = 'Text is too short';

    if (props.valueLength >= 5) {
        message = 'Text is long enough';
    }

    return (
        <div>
            <h2>{message}</h2>
        </div>
    )
}

export default Validation
