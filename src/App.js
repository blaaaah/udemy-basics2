import React, { Component } from 'react';
import './App.css';
import Validation from './components/Validation';
import Char from './components/Char';

class App extends Component {
  state = {
    enteredValue: '',
  }

  inputStatusHandler = (event) => {
    this.setState ({enteredValue: event.target.value});
  }

    deleteCharHandler = (index) => {
    const text = this.state.enteredValue.split('');
    text.splice(index, 1);
    const updatedText = text.join('');
    this.setState({enteredValue: updatedText});

  }

  render() {
    const style = {
      marginTop: '50px',
      border: '2px solid black'
    }

    const charList = this.state.enteredValue.split('').map((ch, index) => {
    return <Char 
      character={ch} 
      key={index}
      clicked={() => this.deleteCharHandler(index)} />;
    })

     return (
      <div className="App">
        <input 
          style={style}
          type="text"
          onChange={this.inputStatusHandler}
          value={this.state.enteredValue} />
        <p>{this.state.enteredValue}</p>
        <Validation valueLength={this.state.enteredValue.length}/>
        {charList}
      </div>
    )
  }
}

export default App
